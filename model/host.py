from model import BaseModel
from sqlalchemy import Column, Integer, String


class HostComputer(BaseModel):
    __tablename__ = 'host_computer'
    id = Column(Integer, primary_key=True)
    name = Column(String(30))
    ip = Column(String(30))
    port = Column(String(10))
    passwd = Column(String(200))
    username = Column(String(200))

    def __repr__(self):
        return "<HostComputer(id='%d',name='%s',passwd='%s',ip='%s',port='%s')>" % (
            self.id, self.name, self.passwd, self.ip, self.port)
