from wtforms.fields import StringField, IntegerField
from wtforms.validators import Required, NumberRange
from wtforms_tornado import Form


class CreateUserForm(Form):
    id = IntegerField(validators=[])
    username = StringField(validators=[Required()])
    passwrod = StringField(validators=[Required()])


class LoginForm(Form):
    username = StringField(validators=[Required()])
    password = StringField(validators=[Required()])
