__author__ = 'JackSong'

from handlers import *


handlers = [
    (r"/", IndexHandler),
    (r"/host", HostHandler),
    (r"/ws", WSHandler)
]
