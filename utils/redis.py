import redis
from tornado.options import options


def get_redis_client():
    pool = redis.ConnectionPool(max_connections=100, wait_for_available=True, host=options.redis_host,
                                port=options.redis_port)
    client = redis.Redis(connection_pool=pool)
    return client
