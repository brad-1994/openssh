from tornado.ioloop import IOLoop


def executor(fn, *args):
    return IOLoop.current().run_in_executor(None, fn, *args)