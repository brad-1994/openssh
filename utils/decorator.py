from functools import wraps
from utils.multithreading import executor
from tornado.stack_context import wrap
from tornado.gen import coroutine


def check_login(f):
    @wraps(f)
    @coroutine
    def wrapper(self, *args, **kwargs):
        auth = self.request.headers.get['authorization']
        if auth is None:
            return self.finish({"status": False, 'response': 'not logged in'})
        cookit = yield executor(self.redis.get, *(auth,))
        if cookit is None:
            return self.finish({"status": False, 'response': 'not logged in'})
        self.user = cookit

    return wrapper
