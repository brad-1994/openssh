import axios from 'axios'

export default {
  getIndexHosts: function (cd) {
    axios.get('/host').then(function (res) {
      cd(res)
    })
  },
  postHost: function (data,callback) {
    var body = new FormData()
    for (let key in data) {
      body.append(key, data[key])
    }
    axios.post('/host', body, {headers: {'Content-Type': 'multipart/form-data'}}).then(res => {
      if (res.status == false) {
        alert(res.data.response)
      }else {
        callback(res)
      }
    })
  },
  delete: function (id) {
    var body = new FormData()
    body.append('id', id)
    axios.delete('/host', {data: body}).then(res => {
      if (res.status == false) {
        alert(res.response)
      }
    })
  }
}
