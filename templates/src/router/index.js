import Vue from 'vue'
import Router from 'vue-router'
import Host from '@/components/Host'
import Term from '@/components/Term'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/hosts',
      name: 'hosts',
      component: Host
    },
    {
      path:'/term/:id',
      name:'term',
      component:Term
    }
  ]
})
