/**
 * Created by linhaifeng on 2016/10/28.
 */
import Vue from 'vue'
import Vuex from 'vuex'
import hosts from '../store/host'

Vue.use(Vuex)
const store = new Vuex.Store({
  modules: {
    hosts: hosts
  }
})
export default store
